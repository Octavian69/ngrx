import { Car } from "../models/Car";

export interface AppState {
    carPage: {
        cars: Car[]
    }
}