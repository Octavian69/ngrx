import * as moment from 'moment';

import { Cars } from "../models/Car";
import { CAR_ACTION, CarsAction } from './cars.action';


const initialState: Cars = {
    cars: []
}


export function carsReducer(state = initialState, action: CarsAction) {
    switch(action.type) {

        case CAR_ACTION.ADD_CAR: {
            state = {
                ...state,
                cars: [...state.cars, action.payload]
            }

            return state;
        }

        case CAR_ACTION.DELETE_CAR: {
            const result = state.cars.filter(car => car.id !== action.payload.id);

            return {
                ...state,
                cars: [...result]
            }
        }

        case CAR_ACTION.UPDATE_CAR: {
            const index = state.cars.findIndex(car => car.id === action.payload.id);
            state.cars[index] = action.payload;
            return {
                ...state,
                cars: [...state.cars]
            }
        }

        case CAR_ACTION.LOAD_CARS: {
            return {
                ...state,
                cars: [...action.payload]
            }
        }

        default: 
            return state;
    }
}