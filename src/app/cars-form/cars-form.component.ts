import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Car } from '../models/Car';
import * as moment from 'moment';
import { CarsService } from '../services/cars.service';

@Component({
  selector: 'app-cars-form',
  templateUrl: './cars-form.component.html',
  styleUrls: ['./cars-form.component.css']
})
export class CarsFormComponent implements OnInit {

  name: FormControl;
  model: FormControl; 

  constructor(private carsService: CarsService) { }

  ngOnInit() {
    this.name = new FormControl('', [Validators.required]);
    this.model= new FormControl('', [Validators.required]);

    console.log(this.name, this.model)
  } 

  loadCars(): void {
    this.carsService.loadCars();
  }

  onAdd(): void {
      const [ name, model ] = [ this.name.value, this.model.value ];
      const date = moment().format('DD.MM.YYYY');
      const car = new Car(name, model, date, false);

      this.carsService.addCar(car);

      this.name.reset();
      this.model.reset();
  }

}
