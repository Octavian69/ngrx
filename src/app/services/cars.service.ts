import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Car } from "../models/Car";
import { Store } from "@ngrx/store";
import { AppState } from "../reducers/app.state";
import { LoadCars, AddCar, DeleteCar, UpdateCar } from "../reducers/cars.action";

@Injectable({
    providedIn: 'root'
})
export class CarsService {
    static BASE_URL = 'http://localhost:3000/';

    constructor(
        private http: HttpClient,
        private store: Store<AppState>
        ) {}

    loadCars(): void {
        this.http.get<Car[]>(CarsService.BASE_URL + 'cars')
        .toPromise()
        .then((cars: Car[]) => {
            this.store.dispatch(new LoadCars(cars));
        })
    }

    addCar(car: Car): void {
        this.http.post<Car>(CarsService.BASE_URL + 'cars', car)
            .subscribe((car: Car) => this.store.dispatch(new AddCar(car)));
    }

    deleteCar(car: Car): void {
        this.http.delete(CarsService.BASE_URL + `cars/${car.id}`)
            .subscribe((response: {}) => {
                this.store.dispatch(new DeleteCar(car));
            })
    }

    updateCar(car: Car): void {
        this.http.patch<Car>(CarsService.BASE_URL + `cars/${car.id}`, car)
            .subscribe((car: Car) => {
                this.store.dispatch(new UpdateCar(car));
            })
    }
}