import { Component, OnInit } from '@angular/core';
import { Cars } from './models/Car';
import { Store } from '@ngrx/store';
import { AppState } from './reducers/app.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'redux';

  carState$: Observable<Cars>;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    // this.store.select('carPage').subscribe((data: Cars) => {
    //   this.cars = data.cars;
    // })

    this.carState$ = this.store.select('carPage');
  }
}
