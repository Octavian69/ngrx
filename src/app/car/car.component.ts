import { Component, OnInit, Input } from '@angular/core';
import { Car } from '../models/Car';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers/app.state';
import { DeleteCar, UpdateCar } from '../reducers/cars.action';
import { CarsService } from '../services/cars.service';


@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  constructor(private carsService: CarsService) { }

  @Input('car') car: Car;

  ngOnInit() {
  }

  onBuy() {
    const update = Object.assign({}, this.car, {isSold: true});
    this.carsService.updateCar(update);
  }

  onDelete(): void {
      this.carsService.deleteCar(this.car);
  }

}
